@extends("layout.index")
@section("content")  
   <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Authorize
                            <small>Grant rights</small>
                        </h1>
                    </div>
                    <div class="col-lg-12">
                        @if(Session::has('message'))
                            <br/>
                            <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                        @endif
                    </div>
                    <!-- /.col-lg-12 -->
                        
                </div>
                <div class="row ">
                    <div class="col-lg-6" style="padding-bottom:15px;padding-left:0px">
                                <div style="text-align:left">
                    
                                    <label><input type="search" class="form-control input-sm" placeholder="abc@gmail.com" name="keyword"></label>
                                    <button class="btn btn-sm btn-primary search"><i class="fa fa-fw fa-search"></i> Tìm kiếm</button>
                                </div>
                    </div>
                    <div id='search'>
                        
                    </div>
                </div>
                <div class="row">
                    @include('authorize.table')
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>

        <script type="text/javascript">
            $('.search').click(function(){
                $.ajaxSetup({
                        headers:{ 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
                     });
                $.ajax({
                        url:'authorize',
                        type: 'post',
                        data: {key_search : $("[type='search']").val(),
                               id_report  : {!! $id_report !!}
                        },
                        success: function(data){
                            $('#search').html(data.html);
                        }
                     });
            });
        </script>
@endsection
