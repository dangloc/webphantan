 <div class="col-lg-7" style="padding-bottom:120px">
                       {!! Form::open( ['url' => "authorize", 'method' => 'POST', 'class' => 'form-horizontal', 'name'=>'uploadform', 'files'=>true] ) !!}
                       		{!! Form::hidden('id_report',$id_report, array('class' => 'form-control')) !!}   
                            @foreach($user_per as $val) 
                                @if($val->id_user != Session::get('user')->id)                  
                            		<div class="form-group">
                            			<label>{{$val->name}}</label>
                            	 		{!! Form::select($val->id_user,['0'=>'Read','1'=>'Read & Write','2'=>'Admin'],$val->permission,array('class' => 'form-control')) !!}
							    	</div>
                                @endif
                            @endforeach
                            <button type="submit" class="btn btn-primary">submit</button>
                       {!! Form::close() !!}
 </div>

