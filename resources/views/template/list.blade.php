@extends("layout.index")
@section("content")
 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Template
                            <small>List</small>
                        </h1>
                    </div>
                    <div class="col-lg-12">
                        @if(Session::has('message'))
                            <br/>
                            <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                        @endif
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-12 data-table">
                        @include("template.table")
                    </div>
                    <div class="col-lg-1 col-lg-offset-9">
                        <a href="template/create"><button type="button" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Add new</button></a>
                    </div>
                    <div class="col-lg-1" style="margin-left:20px">
                        <a href="delete_all_template"><button type="button" class="btn btn-primary delete_all"><i class="glyphicon glyphicon-trash"></i> Delete all</button></a>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>   

        <script type="text/javascript">
            $('.delete_all').click(function(){
                if(!confirm("bạn có chắc chắn muốn xóa hết")){
                    return false;
                }
            });
        </script>
@endsection
