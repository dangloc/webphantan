<div class="col-lg-7" style="padding-bottom:120px">
                       {!! Form::open( ['url' => "information", 'method' => 'POST', 'class' => 'form-horizontal', 'name'=>'uploadform', 'files'=>true] ) !!}
                            <div class="form-group">
                                <label>Name</label>
                                {!! Form::text('name',$user->name, array('class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                {!! Form::email('email',$user->email, array('class' => 'form-control','disabled'=>'disabled')) !!}
                            </div>
                            <div class="form-group">
                            	<label>Change password</label>
                            	{!! Form::checkbox('check','selected',false) !!}
                            </div>
                            <div class="hidden change">

                            <div class="form-group">
                            	<label>Old password</label>
                            	{!!Form::input('password', 'pass_old', '',array('class' => 'form-control'))!!}
                            </div>
                            <div class="form-group">
                            	<label>New password</label>
                            	{!!Form::input('password', 'pass_new', '',array('class' => 'form-control'))!!}
                            </div>
                            <div class="form-group">
                            	<label>Confirm password</label>
                            	{!!Form::input('password', 'pass_confirm', '',array('class' => 'form-control'))!!}
                            </div>

                            </div>
                            

                            <button type="submit" class="btn btn-primary">Update</button>
                       {!! Form::close() !!}
                    </div>

<script type="text/javascript">
	$(':checkbox').change(function(){
		if($('.change').hasClass('hidden')){
			$('.change').removeClass('hidden');
		}else{
			$('.change').addClass('hidden');
		}
	});
</script>