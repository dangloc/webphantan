 <div class="col-lg-7" style="padding-bottom:120px">
                       {!! Form::open( ['url' => "exportfile", 'method' => 'POST', 'class' => 'form-horizontal', 'name'=>'uploadform', 'files'=>true] ) !!}

                            @foreach($conversation as $conver)
                                <div class="form-group">
                                    <label>[{{$conver->username}}] [{{$conver->time}}]

                                        @if($per_user == 1 && $id_user == 
                                        $conver->id_user_conver || $per_user == 2)
                                        <a href="history/{{$conver->id}}">[History edit]</a>
                                        @endif

                                    </label>
                                    @if($per_user == 0)

                                        {!! Form::textarea($conver->id,$conver->content, array('class' => 'form-control  inputHoithoai','disabled'=>'disabled',      
                                        'rows' => 2)) !!}

                                    @elseif($per_user == 1 && $id_user == 
                                        $conver->id_user_conver)

                                        {!! Form::textarea($conver->id,$conver->content, array('class' => 'form-control inputHoithoai','rows' => 2)) !!}
                                        

                                    @elseif($per_user == 1 && $id_user != 
                                        $conver->id_user_conver)
                                         {!! Form::textarea($conver->id,$conver->content, array('class' => 'form-control   inputHoithoai','disabled'=>'disabled',
                                        'rows' => 2)) !!}
                                     
                                    @else
                                        {!! Form::textarea($conver->id,$conver->content, array('class' => 'form-control  inputHoithoai','rows' => 2)) !!}
                                         
                                    @endif  
                                </div>
                            @endforeach
                                
                                <div class="form-group">
                                    <label>Template</label>
                                    {!! Form::select('template',$array_template,null,array('class' => 'form-control template')) !!}
                                </div>
                              
                                <div class="form-group">
                                    <label>Preview</label>
                                    {!!Form::textarea('preview','', array('class' => 'form-control abc', 'id' => 'edit4', 'rows' => 20)) !!}
                                </div>
                                <div class="id_report hidden">
                                    {{$id_report}}
                                </div>                          

                                <div class="form-group">
                                    <button style="margin-top: 8px" type="submit" class="btn btn-primary" id='ajaxx'><i class="glyphicon glyphicon-download"></i> Export PDF</button>
                                </div>
                        {!! Form::close() !!}
                     
                    </div>

 <script type="text/javascript">
            config = {};
            config.language ='en';
            config.width = '800px';
            config.height = '400px';
            CKEDITOR.replace('edit4',config);
 </script>  
 <script type="text/javascript">  
            $('.template').change(function(){
            $.ajaxSetup({
                    headers:{ 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
                 });
            $.ajax({
                    url: "conversation/"+$(this).val(),
                    type: 'get',
                    data: {id_report: $('.id_report').text()},
                    success: function(data){
                        CKEDITOR.instances['edit4'].setData(data);
                    }
            });
        });
 </script>
