<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Title Page</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<style type="text/css">
			.css {
				margin-top: 30px;
			}
		</style>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-4 col-xs-offset-3 css">
					<form action="postlogin" method="POST" role="form">
						<legend style="text-align: center;">Form Login</legend>
						<div class="form-group">
							<label for="">Email</label>
							<input type="email" name="email" class="form-control" id="" placeholder="email">
						</div>
					    <div class="form-group">
					    	<label for=''>Password</label>
					    	<input type="password" name="password" id="inputPas" class="form-control" placeholder="password">
					    </div>
					
						<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-log-in"></i> Login</button>
						<a href="getregister"><button type="button" class="btn btn-primary">Sing Up <i class="glyphicon glyphicon-circle-arrow-right"></i></button></a>    
                        {{csrf_field()}}
					</form>

				</div>
			</div>
			<div class="row">
				<div class="col-xs-4 col-xs-offset-3">
					@if(Session::has('message'))
                            <br/>
                            <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
               		 @endif
                	@if(count($errors)>0)
						@foreach($errors->all() as $er)
						  	<div class="alert alert-warning">
 								 <strong>Thông báo: </strong>{{$er}}
							</div>
						@endforeach
					@endif
				</div>
			</div>
		</div>

		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
 		<script src="Hello World"></script>
	</body>
</html>