@extends("layout.index")
@section("content")
 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row" style="padding-bottom:120px">
                    <div class="col-lg-12">
                        <h1 class="page-header">History
                            <small>List</small>
                        </h1>
                    </div>
                    <div class="col-lg-12">
                        @if(Session::has('message'))
                            <br/>
                            <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                        @endif
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-12 data-table">
                        @include("history.table")
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>   
@endsection
