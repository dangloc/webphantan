-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 28, 2018 lúc 04:43 PM
-- Phiên bản máy phục vụ: 10.1.33-MariaDB
-- Phiên bản PHP: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `webphantan`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `conversation`
--

CREATE TABLE `conversation` (
  `id` int(11) NOT NULL,
  `id_permission` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `conversation`
--

INSERT INTO `conversation` (`id`, `id_permission`, `time`, `content`, `created_at`, `updated_at`) VALUES
(21, 66, '2018-10-26 19:42:43', 'mình tên là Lộc đẹp trai', '2018-10-26 19:42:43', '2018-10-26 19:42:43'),
(22, 67, '2018-10-21 14:32:35', 'mình tên là phương anh', '2018-10-21 14:32:35', '2018-10-21 14:32:35'),
(23, 68, '2018-10-27 15:08:42', 'xin chào bạn m', '2018-10-27 15:08:42', '2018-10-27 15:08:42'),
(24, 69, '2018-10-21 14:31:58', 'hello', '2018-10-21 14:31:58', '2018-10-21 14:31:58'),
(25, 70, '2018-10-17 03:38:51', 'xin chào abc xyz 000', '2018-10-17 03:38:51', '2018-10-17 03:38:51'),
(26, 71, '2018-10-16 09:09:12', 'xin chào xsxscsc', '2018-10-16 09:09:12', '2018-10-16 09:09:12'),
(27, 72, '2018-10-16 09:09:28', 'xin chào fffff', '2018-10-16 09:09:28', '2018-10-16 09:09:28'),
(28, 73, '2017-10-23 13:30:00', 'xin chào', '2018-10-16 09:01:04', '2018-10-16 09:01:04'),
(29, 74, '2017-10-23 13:30:00', 'xin chào', '2018-10-22 00:05:11', '2018-10-22 00:05:11'),
(30, 75, '2017-10-23 13:30:00', 'xin chào', '2018-10-22 00:05:11', '2018-10-22 00:05:11'),
(31, 76, '2017-10-23 13:30:00', 'xin chào', '2018-10-22 00:05:11', '2018-10-22 00:05:11'),
(32, 77, '2017-10-23 13:30:00', 'xin chào', '2018-10-22 00:05:11', '2018-10-22 00:05:11'),
(33, 74, '2017-10-23 13:30:00', 'xin chào', '2018-10-22 00:05:12', '2018-10-22 00:05:12'),
(34, 75, '2017-10-23 13:30:00', 'xin chào', '2018-10-22 00:05:12', '2018-10-22 00:05:12'),
(35, 76, '2017-10-23 13:30:00', 'xin chào', '2018-10-22 00:05:12', '2018-10-22 00:05:12'),
(36, 77, '2017-10-23 13:30:00', 'xin chào', '2018-10-22 00:05:12', '2018-10-22 00:05:12');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `history`
--

CREATE TABLE `history` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_conversation` int(11) NOT NULL,
  `content_edit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `history`
--

INSERT INTO `history` (`id`, `id_user`, `id_conversation`, `content_edit`, `created_at`, `updated_at`) VALUES
(1, 1, 21, 'mình tên là Đặng Xuân Lộc', '2018-10-26 17:43:48', '2018-10-26 17:43:48'),
(2, 1, 21, 'mình tên là Lộc đẹp trai', '2018-10-26 17:56:19', '2018-10-26 17:56:19'),
(3, 3, 23, 'xin chào bạn m', '2018-10-26 18:07:56', '2018-10-26 18:07:56'),
(4, 3, 21, 'mình tên là dangxuanloc96@gmail.com', '2018-10-26 18:09:04', '2018-10-26 18:09:04'),
(5, 3, 21, 'mình tên là dangxuanloc96', '2018-10-26 18:12:07', '2018-10-26 18:12:07'),
(6, 1, 23, 'tôi tên là Đặng Văn Thọ', '2018-10-27 15:08:19', '2018-10-27 15:08:19');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2014_10_12_000000_create_users_table', 1),
(4, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `permission`
--

CREATE TABLE `permission` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_report` int(11) NOT NULL,
  `permission` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `permission`
--

INSERT INTO `permission` (`id`, `id_user`, `id_report`, `permission`, `created_at`, `updated_at`) VALUES
(66, 1, 21, 2, '2018-10-16 03:59:39', '2018-10-15 19:25:41'),
(67, 2, 21, 2, '2018-10-21 14:31:10', '2018-10-15 19:25:41'),
(68, 3, 21, 2, '2018-10-26 18:08:14', '2018-10-15 19:25:41'),
(69, 4, 21, 1, '2018-10-22 13:30:06', '2018-10-15 19:25:42'),
(70, 1, 22, 2, '2018-10-16 09:01:03', '2018-10-16 09:01:03'),
(71, 2, 22, 2, '2018-10-17 03:29:11', '2018-10-16 09:01:03'),
(72, 3, 22, 0, '2018-10-26 15:36:04', '2018-10-16 09:01:03'),
(73, 4, 22, 1, '2018-10-17 15:19:30', '2018-10-16 09:01:03'),
(74, 1, 23, 2, '2018-10-22 00:05:11', '2018-10-22 00:05:11'),
(75, 2, 23, 0, '2018-10-22 00:05:11', '2018-10-22 00:05:11'),
(76, 3, 23, 0, '2018-10-22 00:05:11', '2018-10-22 00:05:11'),
(77, 4, 23, 0, '2018-10-22 00:05:11', '2018-10-22 00:05:11'),
(79, 5, 21, 0, '2018-10-24 06:02:45', '2018-10-24 06:02:45');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `report`
--

CREATE TABLE `report` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `report`
--

INSERT INTO `report` (`id`, `name`, `content`, `created_at`, `updated_at`) VALUES
(21, 'Hệ phân tán', '<p>ddd</p>', '2018-10-16 04:44:21', '2018-10-16 04:44:21'),
(22, 'abcde', '<p>1234</p>', '2018-10-16 19:05:53', '2018-10-16 19:05:53'),
(23, 'nnnnnn', '<p>nnnnnnn</p>', '2018-10-22 00:05:11', '2018-10-22 00:05:11');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `template`
--

CREATE TABLE `template` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `header` text COLLATE utf8_unicode_ci NOT NULL,
  `footer` text COLLATE utf8_unicode_ci NOT NULL,
  `id_user` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `template`
--

INSERT INTO `template` (`id`, `name`, `content`, `header`, `footer`, `id_user`, `created_at`, `updated_at`) VALUES
(4, 'Biên bản cuộc họp', '<p>Bi&ecirc;n bản hợp lớp cntt 2.4 cuối c&ugrave;ng</p>', '<p style=\"text-align:left\">T&Ecirc;N CƠ QUAN, TC CHỦ QUẢN (1)&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong>CỘNG H&Ograve;A X&Atilde; HỘI CHỦ NGHĨA VIỆT NAM</strong></p>\r\n\r\n<p style=\"text-align:left\"><strong>T&Ecirc;N CƠ QUAN, TỔ CHỨC (2)&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Độc lập &ndash; Tự do &ndash; Hạnh ph&uacute;c</strong></p>\r\n\r\n<p style=\"text-align:left\">Số: /BB- ... (3)....</p>\r\n\r\n<h3 style=\"text-align:left\">BI&Ecirc;N&nbsp;BẢN&nbsp;CUỘC HỌP</h3>\r\n\r\n<p style=\"text-align:left\"><strong>..................................(4).................................</strong><br />\r\n_______________</p>\r\n\r\n<p style=\"text-align:left\">Thời gian bắt đầu:.....................................................................................................................</p>\r\n\r\n<p style=\"text-align:left\">Địa điểm:..................................................................................................................................</p>\r\n\r\n<p style=\"text-align:left\">Th&agrave;nh phần tham dự:................................................................................................................</p>\r\n\r\n<p style=\"text-align:left\">...............................................................................................................................................</p>\r\n\r\n<p style=\"text-align:left\">...............................................................................................................................................</p>\r\n\r\n<p style=\"text-align:left\">Chủ tr&igrave; (chủ tọa):.......................................................................................................................</p>\r\n\r\n<p style=\"text-align:left\">Thư k&yacute; (người ghi bi&ecirc;n bản):......................................................................................................</p>\r\n\r\n<p style=\"text-align:left\">Nội dung (theo diễn biến cuộc họp/hội nghị/hội thảo)</p>', '<p>Cuộc họp (hội nghị, hội thảo) kết th&uacute;c v&agrave;o ...... giờ ....., ng&agrave;y ..... th&aacute;ng ..... năm ......./</p>\r\n\r\n<p style=\"text-align:left\">&nbsp;</p>\r\n\r\n<p style=\"text-align:left\"><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;THƯ K&Yacute;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; CHỦ TỌA</strong></p>\r\n\r\n<p style=\"text-align:left\"><em>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;(Chữ k&yacute;)&nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; (Chữ k&yacute;, dấu (nếu c&oacute;))&nbsp; &nbsp;</em></p>\r\n\r\n<p style=\"text-align:left\"><br />\r\n<em>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</em></p>\r\n\r\n<p style=\"text-align:left\"><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Họ v&agrave; t&ecirc;n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Họ v&agrave; t&ecirc;n</strong></p>\r\n\r\n<p style=\"text-align:left\">&nbsp;</p>\r\n\r\n<p style=\"text-align:left\">&nbsp;</p>\r\n\r\n<p style=\"text-align:left\"><em><strong>Nơi nhận:</strong></em><br />\r\n- ..........;<br />\r\n- Lưu: VT, hồ sơ.</p>\r\n\r\n<table align=\"center\" cellspacing=\"0\" style=\"border-collapse:collapse; border-spacing:0px; border:0px; color:rgba(0, 0, 0, 0.87); font-family:arial,helvetica,sans-serif; font-size:16px; font-stretch:inherit; font-variant-east-asian:inherit; font-variant-numeric:inherit; height:24px; line-height:inherit; margin:0px; max-width:100%; overflow:auto; padding:0px; width:874px\">\r\n	<tbody>\r\n	</tbody>\r\n</table>\r\n\r\n<p><em><strong>Nơi nhận:</strong></em><br />\r\n<span style=\"font-family:inherit; font-size:x-small\">- ..........;</span><br />\r\n<span style=\"font-family:inherit; font-size:x-small\">- Lưu: VT, hồ sơ.</span></p>', 1, '2018-10-20 15:51:52', '2018-10-20 15:51:52'),
(5, 'Họp chi bộ liên đoàn', '<p>bbbbbb</p>', '<p>bbbbbb</p>', '<p>dsdsdsdsdsd</p>', 1, '2018-10-20 15:47:42', '2018-10-20 15:47:42');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Đặng Xuân Lộc', 'dangxuanloc@gmail.com', NULL, '$2y$10$tzO186DR2qyIPWE/JNIGt.Jqni0CLBJaCwxaWysOFuuQgEvH2zUZm', 'Xujf4mJvRTVzfAVmcOri6QwginytuLLSMwslt8gBkEaqCkFideDzLejBxyVU', '2018-10-15 14:35:42', '2018-10-18 17:43:39'),
(2, 'Đặng Phương Anh', 'dangphuonganh@gmail.com', NULL, '$2y$10$qh3S6z0PRf4d/OszDs5O0urVxAC4yQHcR6SfeP.V.kApRO1s9MaoO', NULL, NULL, '2018-10-18 04:23:09'),
(3, 'Đặng Văn Thọ', 'dangvantho@gmail.com', NULL, '$2y$10$lqBHUqM68ng9T5DXWnplZOWEmJCZM8Slwkyw6BTHsWWvLvHZk0IrW', NULL, NULL, NULL),
(4, 'Đặng Quốc Phương', 'dangquocphuong@gmail.com', NULL, '$2y$10$19Gl/NRHvOd9OpZX1EfJsepZetPVERPb2PzjDivVzy8MAG6MNhF1C', NULL, NULL, NULL),
(5, 'Đặng Văn A', 'dangxuanloc96@gmail.com', NULL, '$2y$10$aNxh73H0p7GcljukZpQR/udbPPhZujuMCPUwSj.rAIAksYWnM1WKm', NULL, '2018-10-22 15:30:02', '2018-10-22 15:30:02');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `conversation`
--
ALTER TABLE `conversation`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `template`
--
ALTER TABLE `template`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `conversation`
--
ALTER TABLE `conversation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT cho bảng `history`
--
ALTER TABLE `history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `permission`
--
ALTER TABLE `permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT cho bảng `report`
--
ALTER TABLE `report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT cho bảng `template`
--
ALTER TABLE `template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
