<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'content' => 'required',
            'txt' => 'required'
        ];
    }
    public function messages(){
        return [
            'name.required' => 'Bạn chưa nhập tên của report',
            'content.required' => 'Bạn chưa nhập mô tả báo cáo',
            'txt.required' => 'Bạn chưa chọn file'
        ];
    }
}
