<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return 
            [
                'email'=>'required|email',
                'password'=>'required|min:6'
            ];
    }
    public function messages()
    {
        return 
            [
                'email.required' => 'Bạn chưa nhập Email',
                'email.email' => 'Định dạng bạn nhập không phải email',
                'password.required' => 'Bạn chưa nhập password',
                'password.min' => 'Password phải lớn hơn 5 kí tự'
            ];
    }
}