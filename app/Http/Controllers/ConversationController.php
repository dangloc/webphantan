<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Conversation;
use App\Events\SendMessageEvent;
use Session;
use App\Permission;
use App\Template;
use App\History;
class ConversationController extends Controller
{
    public function getConversation(Request $request,$id){
        //lấy template + data conversation
        if($request->ajax()){
            $template = Template::find($id);
            $content = $template->header;
            $id_report = $request->id_report;
             $conversation = DB::table('permission')
            ->join('conversation','conversation.id_permission','permission.id')
            ->join('users','users.id','permission.id_user')
            ->select('users.name as username','conversation.content as content','conversation.time as time','conversation.id as id','permission.permission as permission')
            ->where('permission.id_report',$id_report)
            ->orderBy('time','asc')
            ->get();
            foreach ($conversation as $val) {
                $content.='</br>'.'['.$val->time.']'.$val->username.': '.$val->content.'</br>';
            }
            $content.='</br>'.$template->footer;
            return $content;
        }
        //lấy tât cả conversation của report được lựa chọn
         $conversation = DB::table('permission')
            ->join('conversation','conversation.id_permission','permission.id')
            ->join('users','users.id','permission.id_user')
            ->select('users.name as username','conversation.content as content','conversation.time as time','conversation.id as id','permission.permission as permission','permission.id_user as id_user_conver')
            ->where('permission.id_report',$id)
            ->orderBy('time','asc')
            ->get();
         $id_user = Session::get('user')->id;
         //lấy permisson của user đang dùng so với report vừa chọn,mục đích là xem user hiện tại có quyền sửa hay không
         $permission = Permission::where([['id_report',$id],['id_user',$id_user]])->get();
         $per_user = $permission[0]->permission;
         //lấy list các template covert thành mảng
         $list_template = Template::where('id_user',$id_user)->get();
         $array_template = [];
         foreach ($list_template as $val) {
            $array_template[$val->id] = $val->name;
         }      
         //lấy id_report
         $id_report = $id; 
         return view('conversation.list',compact('conversation','per_user','array_template','id_report','id_user'));
    }
    public function postConversation(Request $request){
         if($request->ajax()){
            //update coversation
            $id_cover = $request->id;
            $new_content = $request->content;
            $update_conver = Conversation::find($id_cover);
            $update_conver->content = $new_content;
            $update_conver->save();
            //create history
            $id_user = Session::get('user')->id;
            $history = new History();
            $history->id_user = $id_user;
            $history->id_conversation = $id_cover;
            $history->content_edit = $new_content;
            $history->save();
            event(new SendMessageEvent($update_conver));
            return ;
         }
    }
}
