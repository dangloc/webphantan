<?php

namespace App\Http\Controllers;
use App\Http\Requests\ReportRequest;
use Illuminate\Http\Request;
use App\Report;
use App\Permission;
use App\Conversation;
use Illuminate\Support\Facades\Auth;
use DB;
use Session;


class ReportController extends Controller
{
      public function index()
    {
    	$id_user=Auth::id();
        $report=DB::table('users')
        ->join('permission','permission.id_user' , 'users.id')
        ->join('report','report.id' , 'permission.id_report')
        ->where('users.id' , $id_user)
        ->get();
        return view('report.list',compact('report'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('report.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReportRequest $request)
    {
        $infor = $request->all();
        $report = new Report();
        $id_report = $report->create($infor)->id;
        if($request->hasFile('txt')){
            $file = $request->file('txt');
            $tail = $file->getClientOriginalExtension();
            if($tail != 'txt' && $tail != 'docx'){
                return redirect('report/create')->with('message','file bạn chọn không phải là file .txt hoặc file .docx');
            }
            $name = $file->getClientOriginalName();
            $new_name = str_random(4).'_'.$name;  
            while(file_exists('upload/sanpham'. $new_name)){
                 $new_name = str_random(4).'_'.$name;  
            }    
            $file->move('upload/sanpham',$new_name);

            $fileOpen=@fopen('upload/sanpham/'.$new_name,'r');
            $contents = "";
            if($tail == 'txt'){
                while(!feof($fileOpen)){
                $contents = $contents.trim(fgets($fileOpen));
                }
            }
            else{
                $contents = read_file_docx('upload/sanpham/'.$new_name);
            }
            $array = explode('[',$contents);
            $collection = [];// mảng các mảng id-user-time-content
            $id_list=[];//mảng các id_user
            for($i = 1;$i < count($array);$i++){
                $vitri = strpos($array[$i],'+');
                $id_user = substr($array[$i],8,$vitri-8);
                $time = substr($array[$i],$vitri+6,19);
                $content = trim(substr($array[$i],$vitri+34),']');
                $collection[] = ['id_user' =>  $id_user,
                 				'time' => $time,
                  				'content'=> $content];
                $id_list[] = $id_user;
            }           
        }
        //tạo các permission
        		$id_user = Session::get('user')->id;
        		$id_list[] = $id_user;
        	foreach (array_unique($id_list) as  $id) {
        		$permission = new Permission();
        		$permission->id_user = $id;
        		$permission->id_report =  $id_report;
        		if($id == $id_user){
        			$permission->permission = 2;
        		}
        		$permission->save();
        	}
        //tạo các conversation
        	   foreach ($collection as $colect) {
        	   	   $permis = DB::table('permission')->where([
        	   	   			['id_user', '=' , $colect['id_user']],
        	   	   			['id_report', '=', $id_report]
        	   	    ])->get();
        	   	   $id_per = $permis[0]->id;
        	   	   $conversation = new Conversation();
        	   	   $conversation->id_permission = $id_per;
        	   	   $conversation->time = $colect['time'];
        	   	   $conversation->content = $colect['content'];
        	   	   $conversation->save();
        	   }
       Session::flash('message','Bạn đã tạo mới thành công một cuộc hội thoại');
       return redirect('report/create');
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $report = Report::find($id);
        return view('report.update',compact('report'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $data = $request->all();
       $report = Report::find($id);
       $report->update($data);
       Session::flash('message',"Bạn đã cập nhật thành công");
       return redirect("report/$id/edit");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
    	$id_user=Session::get('user')->id;
        $report = Report::find($id);
        $report->delete();
        $permission = Permission::where('id_report',$id)->get();
        $id_per_list = [];
        foreach ($permission as  $per) {
        	$id_per_list[] = $per->id;
        	$per->delete();
        }
        foreach ($id_per_list as $val) {
        	$conversation = Conversation::where('id_permission',$val)->get();
        	foreach ($conversation as $value) {
        		$value->delete();
        	}
        }
        Session::flash('message',"Bạn đã xóa thành công");
        return redirect('report');       
    }
}
