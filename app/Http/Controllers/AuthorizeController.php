<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Permission;
use DB;
use Session;

class AuthorizeController extends Controller
{
    public function getAuthorize($id){
    	$id_report = $id;
    	$user_per = DB::table('users')
    		->join('permission','permission.id_user','users.id')
    		->where('id_report',$id)
    		->get();
    	return view('authorize.list',compact('user_per','id_report'));
    }
    public function postAuthorize(Request $request){
    	if($request->ajax()){
    		$key_search = $request->key_search;
    		$id_report = $request->id_report;
    		$users = DB::table('users')->where('email','like','%'.$key_search.'%')->get();
    		return Response::json([
    			 'html' => view('authorize.table_search')
    			 	->with('users',$users)
    			 	->with('id_report',$id_report)
                    ->render(),
    		]);
    	}
    	$array_id = $request->all();
    	$id_report = $array_id['id_report'];
    	foreach ($array_id as $key => $value) {
    		if($key == '_token' || $key == 'id_report'){
    			continue;
    		}
    		$colect_per = DB::table('permission')->where([['id_user',$key],['id_report',$id_report]])->update(['permission' => $value]);
    	}	
    	Session::flash('message','Bạn đã phân quyền thành công');
    	return redirect("authorize/$id_report");
	}
	public function addAuthorize($id_user,$id_report){
		$user = DB::table('permission')->where([['id_user',$id_user],['id_report',$id_report]])->get();
		//kiểm tra xem user thêm vào tồn tại hay chưa
		if(count($user) == 0){
			$permission = new Permission();
			$permission->id_user = $id_user;
			$permission->id_report = $id_report;
			$permission->permission = 0;
			$permission->save();
			Session::flash('message','Bạn vừa thêm một người mới vào cuộc hội thoại này');
		}
		return redirect("authorize/$id_report");
	}
}
