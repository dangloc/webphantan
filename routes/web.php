<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('getlogin','LoginController@getLogin');
Route::post('postlogin','LoginController@postLogin');

Route::get('getlogout',function(){
	if(Session::has('user')){
		Session::flush();
		return redirect('getlogin');
	}
});

Route::get('getregister','RegisterController@getRegister');
Route::post('postregister','RegisterController@postRegister');

Route::group(['middleware' => 'member'],function(){
	Route::resource('report','ReportController');
	Route::get('permission','PermissionController@getPermission');
	Route::post('permission','PermissionController@postPermission');
	Route::get('conversation/{id}','ConversationController@getConversation');
	Route::post('conversation','ConversationController@postConversation');
	Route::get('information','InformationController@getInformation');
	Route::post('information','InformationController@postInformation');
	Route::Resource('template','TemplateController');
	Route::get('delete_all_template','DeleteAllTemplateController@deleteAll');
	Route::get('authorize/{id}','AuthorizeController@getAuthorize');
	Route::post('authorize','AuthorizeController@postAuthorize');
	Route::get('addauthorize/{id_user}/{id_report}','AuthorizeController@addAuthorize');
	Route::get('history/{id}','HistoryController@getHistory');
	Route::get('restore/{id}','HistoryController@getRestore');
	Route::get('delete_history/{id}','HistoryController@deleteHistory');
	Route::get('delete_all_history/{id}','HistoryController@deleteAllHistory');
	Route::post('exportfile','ExportFileController@postExportFile');
});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
	